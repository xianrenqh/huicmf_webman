<?php
/*
* @User: 小灰灰 <762229008@qq.com>
* @Date: 2025-02-20
* @Time: 14:13:40
* @Info:
* Copyright (c) 2022~2025 https://xiaohuihui.cc All rights reserved.
*/

namespace support\lib;

class Form
{

    public static function editor($name, $editorId, $value = '', $style = '', $isload = false)
    {
        $val        = htmlspecialchars_decode($value);
        $editorType = get_config('site_editor');
        $res        = "";
        switch ($editorType) {
            case "wangEditor":
                $res = self::editorWangEditor($name, $editorId, $val, $style, false, $isload);
                break;
            case "aiEditor":
                $res = self::editorAiEditor($editorId, $val, "");
                break;
            case "ueditorPlus":
                $res = self::editorUEditorPlus($name, $editorId, $val, $style, false, $isload);
                break;
        }

        return $res;
    }

    private static function editorWangEditor($name, $editorId, $val, $style, $isMini = false, $isload = false)
    {
        $str = '';
        $str .= '<div style="border: 1px solid #ccc; flex: 1;">
                    <div id="'.$editorId.'-toolbar" style="border-bottom: 1px solid #ccc;"></div>
                    <div id="'.$editorId.'-text-area" style="height: 400px"></div>
                </div>';
        $str .= '<textarea class="layui-textarea" id="'.$editorId.'-content-textarea" name="'.$name.'"
                          style="display: none"></textarea>';

        $str .= '<link href="/app/admin/lib/wangEditor/style.css" rel="stylesheet">';
        $str .= '<script src="/app/admin/lib/wangEditor/index.js"></script>';
        $str .= '<script src="https://cdn.bootcdn.net/ajax/libs/prettier/3.0.3/standalone.min.js"></script>';
        $str .= "<script>
  let E = window.wangEditor;
  editor = E.createEditor({
    selector: '#".$editorId."-text-area',
    html: '<p><br></p>',
    config: {
      placeholder: 'Type here...',
      MENU_CONF: {
        uploadImage: {
          server: '/app/admin/upload/upload?editor_type=wang',
          fieldName: 'custom-field-name',
        }
      }, onChange(editor) {
        html = editor.getHtml()
        document.getElementById('".$editorId."-content-textarea').value = html;
      }
    }
  })

  let toolbar = E.createToolbar({
    editor,
    selector: '#".$editorId."-toolbar',
    config: {}
  })

</script>";

        return $str;
    }

    private static function editorAiEditor($name, $editorId, $val, $style, $isMini = false, $isload = false)
    {
        return <<<HTML
<div id="" style="height:550px; border:2px blue solid;">
  <div>容器可见性测试</div>
</div>

<script type="module">
// 测试1：纯内联模块
console.log("内联模块测试");

// 测试2：动态导入CDN模块
import('https://cdn.jsdelivr.net/npm/vue@3/dist/vue.esm-browser.js')
  .then(() => console.log('CDN模块加载成功'))
  .catch(e => console.error('CDN模块加载失败',e));
</script>
HTML;
    }

    private static function editorUEditorPlus($name, $editorId, $val, $style, $isMini = false, $isload = false)
    {
        $str = '';
        $str .= '';

        return $str;
    }

    public static function getDefaultConfig()
    {
        return [
            'status'  => 0,
            'default' => 'spark',
            'models'  => [
                'spark'  => [
                    'appId'     => '',
                    'apiKey'    => '',
                    'apiSecret' => '',
                    'protocol'  => 'ws',
                    'version'   => 'v3.1'
                ],
                'wenxin' => [
                    'appId'     => '',
                    'apiKey'    => '',
                    'apiSecret' => '',
                    'protocol'  => 'https',
                    'version'   => 'completions'
                ]
            ]
        ];
    }

    public static function getDbConfig()
    {
        $config    = '{"status":"1","default":"spark","models":{"spark":{"appId":"e3e49194","apiSecret":"YTE2OTQwOGUwZjAyN2U0N2Y1ZDlkY2Zk","apiKey":"4c77bba828faf2917418a0ba5651d1ec","protocol":"wss","version":"v4.0"},"wenxin":{"appId":"","apiKey":"","apiSecret":"","protocol":"https","version":"completions"}}}';
        $setConfig = $config ? json_decode($config, true) : self::getDefaultConfig();

        return [
            'status'  => $setConfig['status'] ?? 0,
            'default' => $setConfig['default'] ?? 'spark',
            'models'  => [
                'spark'  => [
                    'appId'     => $setConfig['models']['spark']['appId'] ?? '',
                    'apiKey'    => $setConfig['models']['spark']['apiKey'] ?? '',
                    'apiSecret' => $setConfig['models']['spark']['apiSecret'] ?? '',
                    'protocol'  => $setConfig['models']['spark']['protocol'] ?? 'ws',
                    'version'   => $setConfig['models']['spark']['version'] ?? 'v3.5',
                ],
                'wenxin' => [
                    'appId'     => $setConfig['models']['wenxin']['appId'] ?? '',
                    'apiKey'    => $setConfig['models']['wenxin']['apiKey'] ?? '',
                    'apiSecret' => $setConfig['models']['wenxin']['apiSecret'] ?? '',
                    'protocol'  => $setConfig['models']['wenxin']['protocol'] ?? 'https',
                    'version'   => $setConfig['models']['wenxin']['version'] ?? 'completions',
                ]
            ]
        ];
    }

}
