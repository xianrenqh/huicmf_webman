<?php
//  +----------------------------------------------------------------------
//  | huicmf [ huicmf快速开发框架 ]
//  +----------------------------------------------------------------------
//  | Copyright (c) 2022~2024 https://xiaohuihui.cc All rights reserved.
//  +----------------------------------------------------------------------
//  | Author: 小灰灰 <762229008@qq.com>
//  +----------------------------------------------------------------------
//  | Info:
//  +----------------------------------------------------------------------
//
return [
    'Operation'                        => '操作',
    'Operation successful'             => '操作成功!',
    'Operation failed'                 => '操作失败!',
    'Invalid parameters'               => '未知参数',
    'Unauthorized operation'           => '无权操作',
    'Please enter'                     => '请输入%s%',
    'Are you sure'                     => '您确定要%s%吗？',
    'Add...'                           => '添加%s%',
    'Refresh'                          => '刷新',
    'Status'                           => '状态',
    'Status success'                   => '已启用',
    'Status error'                     => '已禁用',
    'Save'                             => '保存',
    'Verify and Login'                 => '验证并登录',
    'Reset'                            => '重置',
    'Add'                              => '新增',
    'Edit'                             => '编辑',
    'Reload'                           => '重启',
    'Delete'                           => '删除',
    'Log'                              => '日志',
    'Search'                           => '搜索',
    'Clear'                            => '清空',
    'Sort'                             => '排序',
    'CreateTime'                       => '创建时间',
    'UpdateTime'                       => '更新时间',
    'DeleteTime'                       => '删除时间',
    'onetime password'                 => '动态口令',
    'onetime password cannot be empty' => '动态口令不能为空',
    'Man'                              => '男',
    'Woman'                            => '女',
    'Upload image'                     => '上传图片',
    'Choose image'                     => '选择图片',
];
