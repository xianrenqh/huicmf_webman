<?php
/**
 * This file is part of webman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author    walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link      http://www.workerman.net/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

return [
    ''      => [
        \app\middleware\MemoryDetect::class,
    ],
    'api'   => [
        \app\api\middleware\AccessControl::class,
        \app\api\middleware\AuthCheckAccess::class,
    ],
    'admin' => [
        \plugin\admin\app\middleware\AccessControl::class,
        \plugin\admin\app\middleware\SystemLogControl::class,
        \plugin\admin\app\middleware\Lang::class,
    ]
];
