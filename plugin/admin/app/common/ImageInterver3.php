<?php
// +----------------------------------------------------------------------
// | huicmf [ huicmf快速开发框架 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2022~2024 https://xiaohuihui.cc All rights reserved.
// +----------------------------------------------------------------------
// | Author: 小灰灰 <762229008@qq.com>
// +----------------------------------------------------------------------
// | Info: v3版本调用2
// +----------------------------------------------------------------------

namespace plugin\admin\app\common;

use Intervention\Image\ImageManager;
use Intervention\Image\Drivers\Gd\Driver;
use Intervention\Image\Encoders\WebpEncoder;

class ImageInterver3
{

    /**
     * 修改指定图片大小
     *
     * @param $imgSrc   指定图片地址
     * @param $up       是否开启调整
     * @param $maxWidth 指定图片最大宽度
     *
     * @return \Intervention\Image\Image
     */
    public static function editImgSize($imgSrc, $up = false, $maxWidth = 1000, $maxHeight = 1000)
    {
        if ($up === false) {
            return;
        }
        $manager = new ImageManager(new Driver());
        $image   = $manager->read($imgSrc)->scale($maxWidth, $maxHeight);
        $image->save($imgSrc);
    }

    /**
     * 图片裁剪
     *
     * @param $imgSrc   图片路径
     * @param $width    裁剪宽度
     * @param $height
     * @param $x
     * @param $y
     * @param $background
     * @param $position
     *
     * @return void
     */
    public static function cropImgSize(
        $imgSrc,
        $width = 1000,
        $height = 1000,
        $x = 0,
        $y = 0,
        $background = "ffffff",
        $position = "top-left"
    ) {
        $manager   = new ImageManager(new Driver());
        $image     = $manager->read($imgSrc);
        $imgWidth  = $image->width();
        $imgHeight = $image->height();
        $size      = min($imgWidth, $imgHeight);

        $image->crop($size, $size, $x, $y, $background, $position)->resize($width, $height);

        $image->save($imgSrc);
    }

    /**
     * 图片添加水印
     *
     * @param $imgSrc
     * @param $waterImg 水印图片名称
     *
     * @return \Intervention\Image\Image
     */
    public static function addWater($imgSrc, $waterImg = 'mark.png', $position = 'bottom-right', $touming = 100)
    {
        $waterImgSrc = public_path()."/static/water/".$waterImg;
        //处理水印图透明度
        $manager = new ImageManager(new Driver());
        $image   = $manager->read($imgSrc);
        $image->place($waterImgSrc, $position, 0, 0, $touming);
        $image->save($imgSrc);
    }

    /**
     * 图片转webp
     * @return void
     */
    public static function imgToWebp(string $imgSrc, string $ext, int $quality = 90)
    {
        $newPath = str_replace($ext, ".webp", $imgSrc);
        $manager = new ImageManager(new Driver());
        $image   = $manager->read($imgSrc);

        //$encoded = $image->encode(new WebpEncoder(quality: $quality));
        //$image->save($newPath);

        $image->toWebp($quality);
        $image->save($newPath);
        // 删除原图
        if (file_exists($imgSrc)) {
            unlink($imgSrc);
        }

        return $newPath;
    }

    public static function imgSize(string $imgSrc)
    {
        $manager = new ImageManager(new Driver());
        $image   = $manager->read($imgSrc);
        $size    = $image->size();

        return $size;
    }

}
