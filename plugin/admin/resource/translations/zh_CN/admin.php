<?php
//  +----------------------------------------------------------------------
//  | huicmf [ huicmf快速开发框架 ]
//  +----------------------------------------------------------------------
//  | Copyright (c) 2022~2024 https://xiaohuihui.cc All rights reserved.
//  +----------------------------------------------------------------------
//  | Author: 小灰灰 <762229008@qq.com>
//  +----------------------------------------------------------------------
//  | Info:
//  +----------------------------------------------------------------------
//
return [
    'Admin'                                                                        => '管理员',
    'Cannot disable oneself'                                                       => '不能禁用自己',
    'Select at least one role group'                                               => '至少选择一个角色组',
    'Role exceeds permission range'                                                => '角色权限超出范围',
    'The user already exists'                                                      => '该用户名已存在',
    'Role_group'                                                                   => '角色组',
    'Username'                                                                     => '用户名',
    'Nickname'                                                                     => '昵称',
    'Role'                                                                         => '角色',
    'Password'                                                                     => '密码',
    'Email'                                                                        => '邮箱',
    'Mobile'                                                                       => '手机号',
    'Password cannot appear in Chinese'                                            => '密码不能出现中文',
    'Do not update password, please leave blank'                                   => '不更新密码请留空',
    'Composed of letters, numbers, special characters, any two types, 6-15 digits' => '由字母、数字、特殊字符，任意2种组成，6-15位',
    'Last login time'                                                              => '上次登录时间',
    'Last login ip'                                                                => '上次登录IP',
];
