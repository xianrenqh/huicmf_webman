<?php
//  +----------------------------------------------------------------------
//  | huicmf [ huicmf快速开发框架 ]
//  +----------------------------------------------------------------------
//  | Copyright (c) 2022~2024 https://xiaohuihui.cc All rights reserved.
//  +----------------------------------------------------------------------
//  | Author: 小灰灰 <762229008@qq.com>
//  +----------------------------------------------------------------------
//  | Info:
//  +----------------------------------------------------------------------
//

namespace plugin\admin\app\model;

use think\model\concern\SoftDelete;

class User extends Base
{

    use SoftDelete;

    protected $deleteTime = 'delete_time';

    const USER_ROLE = [
        '普通会员',
        'vip会员',
        'svip会员'
    ];

    /**
     * 根据会员Id，判断用户信息是否存在
     *
     * @param string $email
     *
     * @return array
     */
    public function getUserInfoWithId(int $userId): array
    {
        $findUser = $this->where(['id' => $userId])->find();
        if (empty($findUser)) {
            return [];
        } else {
            return is_object($findUser) ? $findUser->toArray() : $findUser;
        }
    }

    /**
     * 根据邮箱，判断用户信息是否存在
     *
     * @param string $email
     *
     * @return array
     */
    public function getUserInfoWithEmail(string $email): array
    {
        $findUser = $this->where(['email' => $email])->find();
        if (empty($findUser)) {
            return [];
        } else {
            return is_object($findUser) ? $findUser->toArray() : $findUser;
        }
    }

    /**
     * 创建用户
     * 第三方授权登录使用
     *
     * @param $param
     *
     * @return int
     */
    public function createUser(array $param): int
    {
        try {
            // 判断对应用户名是否存在，如果存在，不写入用户名
            $findUser = $this->where(['username' => $param['username']])->find();
            if ( ! empty($findUser)) {
                $param['username'] = "";
            }
            $user   = self::create($param);
            $userId = $user->id;

            return $userId;
        } catch (\Exception $e) {
            return 0;
        }

    }

}
