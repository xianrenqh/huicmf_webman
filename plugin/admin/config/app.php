<?php

use support\Request;

return [
    'debug'              => true, //调试模式
    'controller_suffix'  => 'Controller',   //是否开启控制器后缀
    'controller_reuse'   => false,
    //'plugin_market_host' => 'https://webman.xiaohuihui.club',
    'plugin_market_host' => 'https://www.xiaohuihui.club',
    'version'            => '0.2.28'    //如果不想升级，请将版本号变更为最大
];
